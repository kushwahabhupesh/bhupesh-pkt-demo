@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Category</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif
                        <form method="post" action="{{route('category.update', $category->id)}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="PATCH">

                            <div class="form-group">
                                <label for="url">Name:</label>
                                <input cols="5" rows="5" class="form-control" name="cat_name"
                                       value="{{$category->name}}"/>
                            </div>

                            <div class="form-group">
                                <label for="image">Image:</label>
                                <input name="image_name" type="file" accept="image/jpeg, image/png"/>
                            </div>

                            <div class="form-group">
                                <label for="image">Status:</label>
                                <select class="form-control" name="cat_status">
                                    <option value="1" {{$category->status == 1 ? "selected" : ""}}>Yes</option>
                                    <option value="0" {{$category->status == 0 ? "selected" : ""}}>No</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
