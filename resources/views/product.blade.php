@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Product</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if(\Session::has('success'))
                            <div class="alert alert-success">
                                {{\Session::get('success')}}
                            </div>
                        @endif

                        <a class="btn btn-primary" href="{{ route('product.create') }}">{{ __('Add Product') }}</a>

                        <hr/>

                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>id</td>
                                <td>Name</td>
                                <td>product_code</td>
                                <td>quantity</td>
                                <td>price</td>
                                <td>sale_price</td>
                                <td>category_id</td>
                                <td>order</td>
                                <td>status</td>
                                <td colspan="3">Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse($product as $pro)
                                <tr>
                                     <td>
                                         <img src="{{url('uploads/product/'. \App\ProductImage::where('is_feature',1)->where('product_id', $pro->id)->get()->pluck('product_image')->implode('') )}}" width="50" height="50"
                                              alt="{{$pro->name}}"/>
                                     </td>
                                    <td>{{$pro->id}}</td>
                                    <td>{{$pro->name}}</td>
                                    <td>{{$pro->product_code}}</td>
                                    <td>{{$pro->quantity}}</td>
                                    <td>{{$pro->price}}</td>
                                    <td>{{$pro->sale_price}}</td>
                                    <td>
                                        @forelse($pro->category_id as $cat)
                                            {{ \App\Category::where('id','=',$cat)->get()->pluck('name')->implode('') }},
                                        @empty
                                            {{"--"}}
                                        @endforelse
                                    </td>
                                    <td>{{$pro->order}}</td>
                                    <td>{{$pro->status == 1 ? "Yes" : "No" }}</td>
                                    <td><a href="{{route('product.edit',$pro->id)}}" class="btn btn-primary">Edit</a>
                                    </td>
                                    <td>
                                        <form action="{{route('product.destroy', $pro->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                    <td><a href="{{route('product.images',$pro->id)}}" class="btn btn-primary">Images</a>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center"><p>No data found!</p></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        <div class="text-center">
                            {{ $product->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
