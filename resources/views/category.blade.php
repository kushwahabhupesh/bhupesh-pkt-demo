@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Category</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if(\Session::has('success'))
                            <div class="alert alert-success">
                                {{\Session::get('success')}}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-3">
                                <a class="btn btn-primary"
                                   href="{{ route('category.create') }}">{{ __('Add Category') }}</a>
                            </div>
                            <div class="col-md-5">
                                <form action="{{route('category.index')}}" class="d-flex">
                                    <input name="search" class="form-control mr-2" type="text" value="{{$search}}">
                                    <button class="btn btn-primary" type="submit">Filter</button>
                                </form>
                            </div>
                        </div>


                        <hr/>

                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                            <tr>
                                <td>Image</td>
                                <td>Name</td>
                                <td>Added</td>
                                <td>Modified</td>
                                <td>Order</td>
                                <td>Status</td>
                                <td>No. of products</td>
                                <td colspan="2">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($category as $cate)
                                <tr>
                                    <td>
                                        <img src="{{url('uploads/category/'.$cate->image_name)}}" width="50" height="50"
                                             alt="{{$cate->name}}"/>
                                    </td>
                                    <td>{{$cate->name}}</td>
                                    <td>{{$cate->created_at}}</td>
                                    <td>{{$cate->updated_at}}</td>
                                    <td>{{$cate->order}}</td>
                                    <td>{{$cate->status == 1 ? "Yes" : "No" }}</td>
                                    <td>{{"0"}}</td>
                                    <td><a href="{{route('category.edit',$cate->id)}}" class="btn btn-primary">Edit</a>
                                    </td>
                                    <td>
                                        <form action="{{route('category.destroy', $cate->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-center"><p>No data found!</p></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        <div class="text-center">
                            {{ $category->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
