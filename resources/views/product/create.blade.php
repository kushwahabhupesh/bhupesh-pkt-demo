@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Product</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif
                        <form method="post" action="{{ route('product.store') }}" enctype="multipart/form-data">

                            <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                            <div class="form-group">
                                <label for="image">Category:</label>
                                <select class="form-control" name="category_id[]" multiple>
                                    @foreach($category as $cate)
                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="title">Name:</label>
                                <input type="text" class="form-control" name="pro_name"/>
                            </div>

                            <div class="form-group">
                                <label for="title">Quantity:</label>
                                <input type="number" min="0" class="form-control" name="quantity"/>
                            </div>

                            <div class="form-group">
                                <label for="title">price:</label>
                                <input type="number" min="0" class="form-control" name="price"/>
                            </div>

                            <div class="form-group">
                                <label for="title">sale_price:</label>
                                <input type="number" min="0" class="form-control" name="sale_price"/>
                            </div>

                            <div class="form-group">
                                <label for="image">Image:</label>
                                <input name="product_images[]" type="file" accept="image/jpeg, image/png" multiple/>
                            </div>

                            <div class="form-group">
                                <label for="image">Status:</label>
                                <select class="form-control" name="pro_status">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
