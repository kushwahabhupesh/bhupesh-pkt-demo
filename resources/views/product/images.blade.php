@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Product</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif
                        <div class="row">
                            @foreach($images as $img)
                                <div class="col-md-3">
                                    <a href="{{ route('product.feature',[$img->id, $img->product_id] ) }}">
                                        <img src="{{url('uploads/product/'.$img->product_image)}}" class="img-rounded"
                                             alt="{{$img->product_image}}" width="100" height="100"/>
                                        <div class="caption">
                                            <p>Is Feature image : {{$img->is_feature === 0 ? "No" : "Yes" }}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
