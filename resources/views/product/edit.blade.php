@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Product</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif
                        <form method="post" action="{{route('product.update', $product->id)}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="PATCH">

                            <div class="form-group">
                                <label for="image">Category:</label>

                                <select class="form-control" name="category_id[]" multiple>
                                    @foreach($category as $cate)
                                        <option
                                            value="{{$cate->id}}"
                                            selected="{{
                                                in_array($cate->id, $product->category_id) ? "selected" : ""
                                            }}"
                                        >
                                            {{$cate->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="title">Name:</label>
                                <input type="text" class="form-control" name="pro_name" value="{{$product->name}}"/>
                            </div>

                            <div class="form-group">
                                <label for="title">Quantity:</label>
                                <input type="number" min="0" class="form-control" name="quantity"
                                       value="{{$product->quantity}}"/>
                            </div>

                            <div class="form-group">
                                <label for="title">price:</label>
                                <input type="number" min="0" class="form-control" name="price"
                                       value="{{$product->price}}"/>
                            </div>

                            <div class="form-group">
                                <label for="title">sale_price:</label>
                                <input type="number" min="0" class="form-control" name="sale_price"
                                       value="{{$product->sale_price}}"/>
                            </div>

                            <div class="form-group">
                                <label for="image">Image:</label>
                                <input name="product_images[]" type="file" accept="image/jpeg, image/png" multiple/>
                            </div>

                            <div class="form-group">
                                <label for="image">Status:</label>
                                <select class="form-control" name="pro_status">
                                    <option value="1" {{$product->status == 1 ? "selected" : ""}}>Yes</option>
                                    <option value="0" {{$product->status == 0 ? "selected" : ""}}>No</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
