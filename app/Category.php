<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const Cat_STATUS = 1;
    const Cat_INIT_ORDER = 0;

    protected $table = "categories";
    protected $fillable = [
        'name','image_name','order','status'
    ];
/*
    public function product() {
        return $this->hasMany("App\Product");
    }*/
}
