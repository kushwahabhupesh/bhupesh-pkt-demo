<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = "product_images";
    protected $fillable = [
        'product_id', 'product_image', 'is_feature'
    ];

    public function item()
    {
        return $this->belongsTo('App\Product');
    }
}
