<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->query('search');

        if (!empty($search)) {
            $category = Category::where('name', 'like', '%'.$search.'%')->orderBy('id', 'desc')->paginate(10);
        } else {
            $category = Category::latest()->orderBy('id', 'desc')->paginate(10);
        }

        return view('category', compact(['category', 'search']))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        request()->validate([
            'cat_name' => 'required',
            'image_name' => 'required|image|mimes:jpeg,png',
            'cat_status' => 'required',
        ]);

        $category = new Category();
        if ($request->hasFile('image_name')) {
            $categoryImage = $request->file('image_name');
            $extension = $categoryImage->getClientOriginalExtension();

            $new_name = rand().time().'.'.$extension;
            $categoryImage->move(public_path('uploads/category'), $new_name);

            $category->image_name = $new_name;
        }
        $category->name = $request->cat_name;
        $category->order = Category::Cat_INIT_ORDER;
        $category->status = $request->cat_status;
        $category->save();

        return redirect()->route('category.index')
            ->with('success', 'Category created successfully');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $categoryImage = $request->file('image_name');

        if ($categoryImage != "") {
            request()->validate([
                'cat_name' => 'required',
                'cat_status' => 'required',
                'image_name' => 'image|mimes:jpeg,png',
            ]);
        } else {
            request()->validate([
                'cat_name' => 'required',
                'cat_status' => 'required',
            ]);
        }

        $category = Category::find($id);

        if ($request->hasFile('image_name')) {
            $extension = $categoryImage->getClientOriginalExtension();

            $new_name = rand().time().'.'.$extension;
            $categoryImage->move(public_path('uploads/category'), $new_name);

            $category->image_name = $new_name;
        }
        $category->name = $request->cat_name;
        $category->order = Category::Cat_INIT_ORDER;
        $category->status = $request->cat_status;
        $category->save();

        return redirect()->route('category.index')
            ->with('success', 'Category updated successfully');
    }

    public function destroy($id)
    {
        Category::find($id)->delete();

        return redirect()->route('category.index')
            ->with('success', 'Category deleted successfully');
    }
}
