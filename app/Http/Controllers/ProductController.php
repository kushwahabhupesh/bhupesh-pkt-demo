<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::latest()->orderBy('id', 'desc')->paginate(10);

        return view('product', compact('product'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function images($id)
    {
        $images = ProductImage::where('product_id', $id)->get();

        return view('product.images', compact('images'));
    }

    public function feature($id, $product_id)
    {
       ProductImage::where('product_id', $product_id)->update(['is_feature' => 0]);
       ProductImage::where('id', $id)->update(['is_feature' => 1]);

        return redirect()->route('product.index');
    }

    public function create()
    {
        $category = Category::select('id', 'name')->get();

        return view('product.create', compact('category'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'pro_name' => 'required',
            //'product_code' => 'required',
            'quantity' => 'required',
            'price' => 'required',
            'sale_price' => 'required',
            'category_id' => 'required',
            'product_images' => 'required',
            'pro_status' => 'required',
        ]);

        $product = new Product();
        $product->name = $request->pro_name;
        $product->product_code = $product->generateProductCode(10);
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->sale_price = $request->sale_price;
        $product->category_id = is_array($request->category_id) ? implode(',',
            $request->category_id) : $request->category_id;
        $product->order = Product::Product_INIT_ORDER;
        $product->status = $request->pro_status;
        $product->save();

        if ($request->hasFile('product_images')) {
            $productId = $product->id;
            foreach ($request->file('product_images') as $file) {
                $extension = $file->getClientOriginalExtension();
                if (in_array($extension, ['jpeg', 'png'])) {
                    $new_name = rand().time().'_'.$productId.'.'.$extension;
                    $file->move(public_path('uploads/product'), $new_name);

                    ProductImage::create([
                        'product_image' => $new_name,
                        'product_id' => $productId,
                        'is_feature' => 0
                    ]);
                }
            }
        }

        return redirect()->route('product.index')
            ->with('success', 'Product created successfully');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::select('id', 'name')->get();

        return view('product.edit', compact(['product','category']));
    }

    public function update(Request $request, $id)
    {
        $productImage = $request->file('image_name');

        if ($productImage != "") {
            request()->validate([
                'pro_name' => 'required',
                //'product_code' => 'required',
                'quantity' => 'required',
                'price' => 'required',
                'sale_price' => 'required',
                'category_id' => 'required',
                'product_images' => 'required',
                'pro_status' => 'required',
            ]);
        } else {
            request()->validate([
                'pro_name' => 'required',
                //'product_code' => 'required',
                'quantity' => 'required',
                'price' => 'required',
                'sale_price' => 'required',
                'category_id' => 'required',
                'pro_status' => 'required',
            ]);
        }

        $product = Product::find($id);
        $product->name = $request->pro_name;
        $product->product_code = $product->generateProductCode(10);
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->sale_price = $request->sale_price;
        $product->category_id = is_array($request->category_id) ? implode(',',
            $request->category_id) : $request->category_id;
        $product->order = Product::Product_INIT_ORDER;
        $product->status = $request->pro_status;
        $product->save();

        if ($request->hasFile('product_images')) {
            foreach ($request->file('product_images') as $file) {
                $extension = $file->getClientOriginalExtension();
                if (in_array($extension, ['jpeg', 'png'])) {
                    $new_name = rand().time().'_'.$id.'.'.$extension;
                    $file->move(public_path('uploads/product'), $new_name);

                    ProductImage::create([
                        'product_image' => $new_name,
                        'product_id' => $id,
                        'is_feature' => 0
                    ]);
                }
            }
        }

        return redirect()->route('product.index')
            ->with('success', 'Product updated successfully');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->productImages()->delete();
        $product->delete();

        return redirect()->route('product.index')
            ->with('success', 'Product deleted successfully');
    }
}
