<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const Product_STATUS = 1;
    const Product_INIT_ORDER = 0;

    protected $table = "products";
    protected $fillable = [
        'name', 'product_code', 'quantity', 'price', 'sale_price', 'category_id', 'order', 'status'
    ];

    public function generateProductCode($number)
    {
        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$number.time();
        $sess = substr($sess, -20);

        return $sess;
    }

   /* public function category() {
        return $this->belongsTo("App\Category");
    }*/

    public function productImages() {
        return $this->hasMany("App\ProductImage");
    }

    public function getCategoryIdAttribute($value) {
        return explode(",", $value);
    }
}
