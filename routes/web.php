<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::group(['prefix' => '/', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        //return view('welcome');
        return redirect()->route("home");
    });

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('product', 'ProductController');
    Route::get('product/images/{id}', 'ProductController@images')->name('product.images');
    Route::get('product/feature/{id}/{product_id}', 'ProductController@feature')->name('product.feature');
    Route::resource('category', 'CategoryController');
});
